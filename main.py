from index import Index

if __name__ == '__main__':
    nb_documents = input(
        "De combien de documents voulez-vous extraire le titre ?")

    index = Index(nb_documents)

    index.extract_title()
    index.statistics()
    index.index_tojson()
    index.metadata_tojson()
