import json
import socket
from typing import List
from bs4 import BeautifulSoup
import pandas as pd
import requests
from nltk.tokenize import word_tokenize


class Index():

    def __init__(self, nb_documents):
        self.nb_documents = nb_documents
        self.listUrls = []
        self.list_title = []
        self.index = pd.DataFrame(
            columns=['tokens', 'documents'])
        self.metadata = {'Nombre de tokens distincts': 0, 'Nombre de documents': 0,
                         'Total de tokens': 0, 'Nombre de tokens moyen par document': 0}

    def extract_title(self):
        file = open('crawled_urls.json')
        list_urls = json.load(file)
        # list_title = []

        socket.setdefaulttimeout(3)
        for url in list_urls[:min(int(self.nb_documents), len(list_urls))]:
            try:
                request = requests.get(url, timeout=5)
            except:
                continue
            soup = BeautifulSoup(request.content, 'html.parser')
            title = soup.title.string
            if title is not None:
                self.list_title.append(str(title))
            else:
                self.list_title.append('')

        # json_content = json.dumps(list_title, ascii=False)

        # with open("title_webpages.json", "w") as file:
        #     file.write(json_content)

    def add_to_index(self, list_tokens: list[str], id_doc: int):
        for token in list_tokens:
            # Booléen indiquant si le token est dans l'index ou non (on part du principe que non)
            isToken = False

            for i in range(self.index.shape[0]):
                if token == self.index.iloc[i, 0]:
                    isToken = True
                    if str(id_doc) in self.index.iloc[i, 1].keys():
                        self.index.iloc[i, 1][str(id_doc)] += 1
                    else:
                        self.index.iloc[i, 1][str(id_doc)] = 1
                        break

            if isToken == False:
                self.index.loc[self.index.shape[0]] = [token, {str(id_doc): 1}]

    def statistics(self):
        self.metadata['Nombre de documents'] = len(self.list_title)

        for i in range(len(self.list_title)):
            title = self.list_title[i]
            list_word = title.split()
            list_tokens = [token.lower() for token in list_word]

            self.metadata['Total de tokens'] += len(list_tokens)

            self.add_to_index(list_tokens, i)

        self.metadata['Nombre de tokens distincts'] = self.index.shape[0]
        try:
            self.metadata['Nombre de tokens moyen par document'] = round(
                self.metadata['Total de tokens'] / self.metadata['Nombre de documents'])
        except:
            self.metadata['Nombre de tokens moyen par document'] = 0

    def index_tojson(self):
        self.index.to_json('title.non_pos_index.json')

    def metadata_tojson(self):
        json_content = json.dumps(self.metadata, ensure_ascii=False)
        with open('metadata.json', 'w') as file:
            file.write(json_content)
