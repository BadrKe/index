# Index

## Description

Ce projet fait office de rendu pour le second TP d'Indexation Web.

## Installation

Afin de pouvoir utiliser ce programme, veuillez installer les dépendances nécessaires.

```
git clone https://gitlab.com/BadrKe/index.git
cd index
pip install -r requirements.txt
```

## Lancement du programme

Pour lancer le programme, veuillez tapez les lignes de commandes suivantes dans votre terminal.

```
cd index
python3 main.py crawled_urls.json
```

Puis veuillez renseigner dans le terminal le nombre de documents dont vous voulez extraire leur titre.

## Auteur

Badr KEBRIT